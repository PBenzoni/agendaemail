package it.uninsubria.agendaemail

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton

class SendEmailActivity : AppCompatActivity() {
    private var sendComplete = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_email)

        val recipientLabel = findViewById<EditText>(R.id.etRecipient)
        val subjectLabel = findViewById<EditText>(R.id.etSubject)
        val messageLabel = findViewById<EditText>(R.id.etMessage)
        val subject = intent.getStringExtra("SUBJECT")  //legge l'oggetto di default, che cambia in base all'operazione da notificare

        sendComplete = false

        subjectLabel.setText(subject)

        findViewById<FloatingActionButton>(R.id.buttonCancel).setOnClickListener {
            val builder = AlertDialog.Builder(this)
            with(builder){
                setMessage(resources.getText(R.string.dialog_send_cancel))
                setPositiveButton(resources.getText(R.string.dialog_button_ok)) { dialog, id ->
                    //torna alla main activity
                    startActivity(Intent(applicationContext, MainActivity::class.java))
                }
                setNegativeButton(resources.getText(R.string.dialog_button_cancel), null)
                show()
            }
        }

        findViewById<FloatingActionButton>(R.id.buttonConfirm).setOnClickListener {
            //se i campi necessari sono compilati
            if(recipientLabel.text.toString() != "" && messageLabel.text.toString() != "") {
                sendEmail(recipientLabel.text.toString(), subjectLabel.text.toString(), messageLabel.text.toString())
                sendComplete = true
            }
        else
            Toast.makeText(applicationContext, resources.getText(R.string.dialog_registered_ko), Toast.LENGTH_LONG).show()
        }
    }

    private fun sendEmail(recipient: String, subject: String, message: String){
        //avvia un'activity che permette di scegliere con quale applicazione inviare l'email
        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(recipient))
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        intent.putExtra(Intent.EXTRA_TEXT, message)
        intent.type = "message/rfc822"
        startActivity(Intent.createChooser(intent, "Select email"))
    }

    override fun onResume() {
        super.onResume()
        if (sendComplete) {
            Toast.makeText(applicationContext, resources.getText(R.string.dialog_send_ok), Toast.LENGTH_LONG).show()
            //torna alla main activity
            startActivity(Intent(applicationContext, MainActivity::class.java))
        }
    }
}