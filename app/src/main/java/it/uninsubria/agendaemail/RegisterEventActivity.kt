package it.uninsubria.agendaemail

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*

class RegisterEventActivity : AppCompatActivity() {

    lateinit var id : String
    lateinit var name : String
    private lateinit var startDate : String
    private lateinit var endDate : String
    private lateinit var startTime : String
    private lateinit var endTime : String
    private lateinit var description : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_event)

        val db = DataBaseHelper(this)

        val function = intent.getStringExtra("FUNCTION")

        val today = Calendar.getInstance()
        var date = intent.getStringExtra("DATE")
        val dateFormat = DateTimeFormatter.ofPattern("d/M/yyyy")
        val timeFormat = DateTimeFormatter.ofPattern("H:m")

        val nameLabel = findViewById<EditText>(R.id.etEventName)
        val startDateLabel = findViewById<TextView>(R.id.twStartDate)
        val endDateLabel = findViewById<TextView>(R.id.twEndDate)
        val startTimeLabel = findViewById<TextView>(R.id.twStartTime)
        val endTimeLabel = findViewById<TextView>(R.id.twEndTime)
        val descriptionLabel = findViewById<EditText>(R.id.etDescription)

        var defaultSubject = resources.getText(R.string.subject_event_created)
        findViewById<TextView>(R.id.tvTitle).text = getResources().getText(R.string.title_new_event)

        if (date.isNullOrEmpty())
            date = today.get(Calendar.YEAR).toString() + "/" + (today.get(Calendar.MONTH) + 1).toString() + "/" + today.get(Calendar.DAY_OF_MONTH).toString()

        //assegna alle TextView i valori della data selezionata e dell'ora corrente del sistema
        startDateLabel.text = date
        endDateLabel.text = date
        startTimeLabel.text = dateTimeFormatter(LocalDateTime.now().format(timeFormat), ":")
        endTimeLabel.text = dateTimeFormatter(LocalDateTime.now().format(timeFormat), ":")

        if (function.equals("Update")){
            //se la funzione scelta è la modifica di un evento già esistente
            //assegna ai campi i valori dell'evento da modificare
            findViewById<TextView>(R.id.tvTitle).text = getResources().getText(R.string.title_update_event)
            id = intent.getStringExtra("ID").toString()
            val event = db.selectEvent(id)
            nameLabel.setText(event.name)
            startDateLabel.text = event.startDate
            endDateLabel.text = event.endDate
            startTimeLabel.text = event.startTime
            endTimeLabel.text = event.endTime
            descriptionLabel.setText(event.description)
            defaultSubject = resources.getText(R.string.subject_event_updated)
        }

        updateValues()

        startDateLabel.setOnClickListener{
            //mostra in un dialog l'interfaccia per la selezione della data
            val builder = AlertDialog.Builder(this)
            with(builder) {
                val view = layoutInflater.inflate(R.layout.fragment_datepicker, null)
                setView(view)
                val datePicker = view.findViewById<DatePicker>(R.id.dpDate)
                var selectedDate = startDate
                val dateToSet = startDate.split("/")

                datePicker.init(dateToSet[2].toInt(), dateToSet[1].toInt() - 1, dateToSet[0].toInt())
                { view, year, month, day ->
                    val month = month + 1
                    selectedDate = dateTimeFormatter("$day/$month/$year", "/")
                }
                setPositiveButton(resources.getText(R.string.dialog_button_ok)) { dialog, id ->
                    //assegna alla TextView della data d'inizio il valore selezionato
                    startDateLabel.text = selectedDate
                    //se la data d'inizio è maggiore della data di fine aggiorna anche quest'ultima
                    if (LocalDate.parse(selectedDate, dateFormat) > LocalDate.parse(endDate, dateFormat)) {
                        endDateLabel.text = selectedDate
                        //se le date di inizio e fine sono uguali, l'ora di fine deve essere maggiore o uguale all'ora di inizio
                        if (LocalTime.parse(startTime, timeFormat) > LocalTime.parse(endTime, timeFormat))
                            endTimeLabel.text = startTime
                    }
                    updateValues()
                }
                setNegativeButton(resources.getText(R.string.dialog_button_cancel), null)
                show()
            }
        }

        endDateLabel.setOnClickListener{
            //mostra in un dialog l'interfaccia per la selezione della data
            val builder = AlertDialog.Builder(this)
            with(builder) {
                val view = layoutInflater.inflate(R.layout.fragment_datepicker, null)
                setView(view)
                val datePicker = view.findViewById<DatePicker>(R.id.dpDate)
                var selectedDate = endDate
                val dateToSet = endDate.split("/")

                datePicker.init(dateToSet[2].toInt(), dateToSet[1].toInt() - 1, dateToSet[0].toInt())
                { view, year, month, day ->
                    val month = month + 1
                    selectedDate = dateTimeFormatter("$day/$month/$year", "/")
                }
                setPositiveButton(resources.getText(R.string.dialog_button_ok)) { dialog, id ->
                    //se la data scelta è maggiore o uguale alla data d'inizio, aggiorna la TextView
                    if (LocalDate.parse(selectedDate, dateFormat) >= LocalDate.parse(startDate, dateFormat))
                        endDateLabel.text = selectedDate
                    //se le date di inizio e fine sono uguali, l'ora di fine deve essere maggiore o uguale all'ora di inizio
                    if (LocalDate.parse(startDate, dateFormat) == LocalDate.parse(selectedDate, dateFormat))
                        if (LocalTime.parse(startTime, timeFormat) > LocalTime.parse(endTime, timeFormat))
                            endTimeLabel.text = startTime
                    updateValues()
                }
                setNegativeButton(resources.getText(R.string.dialog_button_cancel), null)
                show()
            }
        }

        startTimeLabel.setOnClickListener{
            //mostra in un dialog l'interfaccia per la selezione dell'ora
            val builder = AlertDialog.Builder(this)
            with(builder) {
                val view = layoutInflater.inflate(R.layout.fragment_timepicker, null)
                setView(view)
                val timePicker = view.findViewById<TimePicker>(R.id.tpTime)
                var selectedTime = startTime
                timePicker.hour = startTime.split(":")[0].toInt()
                timePicker.minute = startTime.split(":")[1].toInt()
                timePicker.setIs24HourView(true)

                timePicker.setOnTimeChangedListener { view, hour, minute ->
                    selectedTime = dateTimeFormatter("$hour:$minute", ":")
                }
                setPositiveButton(resources.getText(R.string.dialog_button_ok)) { dialog, id ->
                    //se le date di inizio e fine sono uguali, l'ora di fine deve essere maggiore o uguale all'ora di inizio
                    if (LocalDate.parse(startDate, dateFormat) == LocalDate.parse(endDate, dateFormat))
                        if (LocalTime.parse(selectedTime, timeFormat) > LocalTime.parse(endTime, timeFormat))
                            endTimeLabel.text = selectedTime
                    startTimeLabel.text = selectedTime
                    updateValues()
                }
                setNegativeButton(resources.getText(R.string.dialog_button_cancel), null)
                show()
            }
        }

        endTimeLabel.setOnClickListener{
            //mostra in un dialog l'interfaccia per la selezione dell'ora
            val builder = AlertDialog.Builder(this)
            with(builder) {
                val view = layoutInflater.inflate(R.layout.fragment_timepicker, null)
                setView(view)
                val timePicker = view.findViewById<TimePicker>(R.id.tpTime)
                timePicker.hour = endTime.split(":")[0].trim().toInt()
                timePicker.minute = endTime.split(":")[1].trim().toInt()
                var selectedTime = endTime
                timePicker.setIs24HourView(true)

                timePicker.setOnTimeChangedListener { view, hour, minute ->
                    selectedTime = dateTimeFormatter("$hour:$minute", ":")
                }
                setPositiveButton(resources.getText(R.string.dialog_button_ok)) { dialog, id ->
                    //se le date di inizio e fine sono uguali, l'ora di fine deve essere maggiore o uguale all'ora di inizio
                    if (LocalDate.parse(startDate, dateFormat) == LocalDate.parse(endDate, dateFormat))
                        if (LocalTime.parse(startTime, timeFormat) > LocalTime.parse(selectedTime, timeFormat))
                            selectedTime = startTime
                    endTimeLabel.text = selectedTime
                    updateValues()
                }
                setNegativeButton(resources.getText(R.string.dialog_button_cancel), null)
                show()
            }
        }

        findViewById<FloatingActionButton>(R.id.buttonCancel).setOnClickListener {
            val builder = AlertDialog.Builder(this)
            with(builder){
                setMessage(resources.getText(R.string.dialog_cancel))
                setPositiveButton(resources.getText(R.string.dialog_button_ok)) { dialog, id ->
                    //torna alla main activity
                    startActivity(Intent(applicationContext, MainActivity::class.java))
                }
                setNegativeButton(resources.getText(R.string.dialog_button_cancel), null)
                show()
            }
        }

        findViewById<FloatingActionButton>(R.id.buttonConfirm).setOnClickListener {
            //se i campi principali sono stati compilati
            if (nameLabel.text.toString() != "" && startDateLabel.text.toString() != "" && startTimeLabel.text.toString() != "" && endDateLabel.text.toString() != "" && endTimeLabel.text.toString() != ""){
                updateValues()
                //se la funzione richiesta è creare un nuovo evento
                if (function.equals("New")) {
                    //inserisce un nuovo elemento nel db
                    db.insert(name, startDate, endDate, startTime, endTime, description)
                //se la funzione richiesta è modificare un evento esistente
                } else if (function.equals("Update")){
                    //aggiorna l'elemento nel db corrispondente con i nuovi valori
                    db.update(id, name, startDate, endDate, startTime, endTime, description)
                }
                val builder = AlertDialog.Builder(this)
                with(builder) {
                    setMessage(resources.getText(R.string.dialog_registered_ok))
                    setPositiveButton(resources.getText(R.string.dialog_button_ok)) { dialog, thisId ->
                        //avvia l'activity per inviare un'email di notifica, passando anche l'oggetto di default
                        val intent = Intent(applicationContext, SendEmailActivity::class.java)
                        //intent.putExtra("SUBJECT", defaultSubject.toString() + name)
                        intent.putExtra("SUBJECT", "$defaultSubject$name")
                        startActivity(intent)
                    }
                    setNegativeButton(resources.getText(R.string.dialog_button_cancel)) { dialog, id ->
                        //torna alla main activity
                        startActivity(Intent(applicationContext, MainActivity::class.java))
                    }
                    show()
                }
            }else{
                Toast.makeText(applicationContext, resources.getText(R.string.dialog_registered_ko), Toast.LENGTH_LONG).show()
            }
        }
    }

   private fun dateTimeFormatter(toFormat: String, separator: String): String{
       //funzione per formattare le stringhe di data e ora nel formato dd/MM/yyyy hh:mm
        val temp = toFormat.split(separator)
        var formatted = ""
        for((i, e) in temp.withIndex()) {
            formatted += if (e.toInt() < 10) //per avere sempre valori a doppia cifra
                "0$e"
            else
                e
            if (i < temp.lastIndex)
                formatted += separator
        }
        return formatted
    }

   private fun updateValues(){
       //funzione per aggiornare i valori delle variabili d'appoggio con i valori inseriti dall'utente
        name = findViewById<EditText>(R.id.etEventName).text.toString()
        startDate = findViewById<TextView>(R.id.twStartDate).text.toString()
        startTime = findViewById<TextView>(R.id.twStartTime).text.toString()
        endDate = findViewById<TextView>(R.id.twEndDate).text.toString()
        endTime = findViewById<TextView>(R.id.twEndTime).text.toString()
        description = findViewById<EditText>(R.id.etDescription).text.toString()
    }
}
