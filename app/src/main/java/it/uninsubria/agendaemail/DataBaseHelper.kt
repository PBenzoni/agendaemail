package it.uninsubria.agendaemail

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

const val DATABASE_VERSION = 1
const val DATABASE_NAME = "sqlite_data.db"
const val TABLENAME = "events"
const val COL_ID = "id"
const val COL_NAME = "name"
const val COL_STARTDATE = "start_date"
const val COL_ENDDATE = "end_date"
const val COL_STARTTIME = "start_time"
const val COL_ENDTIME = "end_time"
const val COL_DESCRIPTION = "description"

class DataBaseHelper(var context: Context): SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION){
    override fun onCreate(db: SQLiteDatabase?) {
        //query per creare la TABLE
        val createTable = "CREATE TABLE " + TABLENAME + "(" +
                COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COL_NAME + " VARCHAR(128)," +
                COL_STARTDATE + " VARCHAR(10)," +
                COL_ENDDATE + " VARCHAR(10)," +
                COL_STARTTIME + " VARCHAR(10)," +
                COL_ENDTIME + " VARCHAR(10)," +
                COL_DESCRIPTION + " VARCHAR(512)" + ")"
        db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase, newVersion: Int, oldVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLENAME")
        onCreate(db)
    }

    fun insert(name: String, startDate: String, endDate: String, startTime: String, endTime: String, description: String?) {
        //funzione per effettuare la INSERT di un nuovo elemento nella TABLE
        val contentValues = ContentValues()
        contentValues.put(COL_NAME, name)
        contentValues.put(COL_STARTDATE, startDate)
        contentValues.put(COL_ENDDATE, endDate)
        contentValues.put(COL_STARTTIME, startTime)
        contentValues.put(COL_ENDTIME, endTime)
        contentValues.put(COL_DESCRIPTION, description)
        val db = this.writableDatabase
        db.insert(TABLENAME, null, contentValues)
        db.close()
    }

    fun update(id: String, name: String, startDate: String, endDate: String, startTime: String, endTime: String, description: String?){
        //funzione per effettuare l'UPDATE dei valori di una tupla nella TABLE
        val contentValues = ContentValues()
        val db = this.readableDatabase
        contentValues.put(COL_NAME, name)
        contentValues.put(COL_STARTDATE, startDate)
        contentValues.put(COL_ENDDATE, endDate)
        contentValues.put(COL_STARTTIME, startTime)
        contentValues.put(COL_ENDTIME, endTime)
        contentValues.put(COL_DESCRIPTION, description)
        db.update(TABLENAME, contentValues,"$COL_ID=?", Array(1){id})
        db.close()
    }

    fun remove(id: String){
        //funzione per eliminare una tupla dalla TABLE
        val db = this.readableDatabase
        db.delete(TABLENAME, "$COL_ID=?", Array(1){id})
        db.close()
    }

    fun readData(selectedDate: String): ArrayList<DataEvent>{
        //funzione per leggere i valori di tutte le tuple che hanno STARTDATE o ENDDATE uguali alla stringa richiesta
        val list: ArrayList<DataEvent> = ArrayList()
        val db = this.readableDatabase
        val query = "Select * from $TABLENAME where $COL_STARTDATE = '$selectedDate' OR $COL_ENDDATE = '$selectedDate'"
        val cursor = db.rawQuery(query, null)
        val idIndex = cursor.getColumnIndex(COL_ID)
        val nameIndex = cursor.getColumnIndex(COL_NAME)
        val startDateIndex = cursor.getColumnIndex(COL_STARTDATE)
        val endDateIndex = cursor.getColumnIndex(COL_ENDDATE)
        val startTimeIndex = cursor.getColumnIndex(COL_STARTTIME)
        val endTimeIndex = cursor.getColumnIndex(COL_ENDTIME)
        val descriptionIndex = cursor.getColumnIndex(COL_DESCRIPTION)
        if (cursor.moveToFirst()){
            do {
                val dataEvent = DataEvent()
                dataEvent.id = cursor.getString(idIndex)
                dataEvent.name = cursor.getString(nameIndex)
                dataEvent.startDate = cursor.getString(startDateIndex)
                dataEvent.endDate = cursor.getString(endDateIndex)
                dataEvent.startTime = cursor.getString(startTimeIndex)
                dataEvent.endTime = cursor.getString(endTimeIndex)
                dataEvent.description = cursor.getString(descriptionIndex)
                list.add(dataEvent)
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()
        return list
    }

    fun selectEvent(id: String): DataEvent{
        //funzione per leggere i valori della tupla con ID corrispondente a quello richiesto
        val dataEvent = DataEvent()
        val db = this.readableDatabase
        val query = "Select * from $TABLENAME where $COL_ID = '$id' "
        val cursor = db.rawQuery(query, null)
        val idIndex = cursor.getColumnIndex(COL_ID)
        val nameIndex = cursor.getColumnIndex(COL_NAME)
        val startDateIndex = cursor.getColumnIndex(COL_STARTDATE)
        val endDateIndex = cursor.getColumnIndex(COL_ENDDATE)
        val startTimeIndex = cursor.getColumnIndex(COL_STARTTIME)
        val endTimeIndex = cursor.getColumnIndex(COL_ENDTIME)
        val descriptionIndex = cursor.getColumnIndex(COL_DESCRIPTION)
        if (cursor.moveToFirst()){
                dataEvent.id = cursor.getString(idIndex)
                dataEvent.name = cursor.getString(nameIndex)
                dataEvent.startDate = cursor.getString(startDateIndex)
                dataEvent.endDate = cursor.getString(endDateIndex)
                dataEvent.startTime = cursor.getString(startTimeIndex)
                dataEvent.endTime = cursor.getString(endTimeIndex)
                dataEvent.description = cursor.getString(descriptionIndex)
        }
        cursor.close()
        db.close()
        return dataEvent
    }
}