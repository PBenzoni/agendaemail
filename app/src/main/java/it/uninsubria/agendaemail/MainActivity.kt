package it.uninsubria.agendaemail

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.CalendarView
import android.widget.ListView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.util.*

class MainActivity : AppCompatActivity() {
    private lateinit var eventList: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val calendar = findViewById<CalendarView>(R.id.calendarView)
        eventList = findViewById<ListView>(R.id.registeredEvents)
        val today = Calendar.getInstance()
        var date = dateTimeFormatter(today.get(Calendar.DAY_OF_MONTH).toString() + "/" +
                (today.get(Calendar.MONTH) + 1).toString() + "/" + today.get(Calendar.YEAR).toString(), "/")

        val db = DataBaseHelper(this)
        var events = db.readData(date)
        var eventAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, events)
        eventList.adapter = eventAdapter

        eventList.setOnItemClickListener { parent, view, pos, itemId ->
            //avvia l'activity di visualizzazione dell'evento, comunicando l'id dell'evento selezionato
            if(eventList.adapter.getItem(pos) != null) {
                val intent = Intent(applicationContext, DisplayEventActivity::class.java)
                intent.putExtra("ID", eventAdapter.getItem(pos)!!.id)
                startActivity(intent)
            }
        }

        calendar.setOnDateChangeListener { cv, y, m, d ->
            //legge dal db gli eventi che occorrono nella data selezionata
            date = dateTimeFormatter(d.toString() + "/" + (m + 1).toString() + "/" + y.toString(), "/")
            events = db.readData(date)
            eventAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, events)
            eventList.adapter = eventAdapter
        }

        findViewById<FloatingActionButton>(R.id.btnNew).setOnClickListener {
            //avvia l'activity di creazione/modifica eventi specificando che si tratta di un'operazione di creazione nuovo evento
            val intent = Intent(applicationContext, RegisterEventActivity::class.java)
            intent.putExtra("FUNCTION", "New")
            intent.putExtra("DATE", date) //comunica la data in cui si vuole inserire il nuovo evento
            startActivity(intent)
        }

        //imposta come immagine del bottone il giorno corrente
        findViewById<FloatingActionButton>(R.id.btnToday).setImageBitmap(textAsBitmap(today.get(Calendar.DAY_OF_MONTH).toString(), 40.toFloat(), Color.WHITE))

        findViewById<FloatingActionButton>(R.id.btnToday).setOnClickListener {
            //imposta sul calendario la data corrente e recupera gli eventi corrispondenti dal db
            calendar.setDate(
                System.currentTimeMillis(),
                false,
                true
            )
            date = dateTimeFormatter(today.get(Calendar.DAY_OF_MONTH).toString() + "/"
                    + (today.get(Calendar.MONTH) + 1).toString() + "/"
                    + today.get(Calendar.YEAR).toString(), "/")
            events = db.readData(date)
            eventAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, events)
            eventList.adapter = eventAdapter
        }
    }

   private fun dateTimeFormatter(toFormat: String, separator: String): String{
       //funzione per formattare le stringhe di data e ora nel formato dd/MM/yyyy hh:mm
       val temp = toFormat.split(separator)
       var formatted = ""
       for((i, e) in temp.withIndex()) {
            formatted += if (e.toInt() < 10) //per avere sempre valori a doppia cifra
                "0$e"
            else
                e
            if (i < temp.lastIndex)
                formatted += separator
       }
       return formatted
    }

    private fun textAsBitmap(text: String, textSize: Float, textColor: Int): Bitmap{
        //funzione per disegnare il giorno corrente su una bitmap
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.textSize = textSize
        paint.color = textColor
        paint.textAlign = Paint.Align.LEFT
        val baseLine = -paint.ascent()
        val width = (paint.measureText(text) + 0.0f).toInt()
        val height = (baseLine + paint.descent() + 0.0f).toInt()
        val image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(image)
        canvas.drawText(text, 0F, baseLine, paint)
        return image
    }
}