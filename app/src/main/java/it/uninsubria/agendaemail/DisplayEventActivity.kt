package it.uninsubria.agendaemail

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.google.android.material.floatingactionbutton.FloatingActionButton

class DisplayEventActivity:AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display_event)

        val id = intent.getStringExtra("ID").toString() //legge il valore dell'id dell'oggetto selezionato
        val db = DataBaseHelper(this)

        val defaultSubject = resources.getText(R.string.subject_event_deleted)   //valore di default per l'oggetto dell'email di notifica

        val tvDescription = findViewById<TextView>(R.id.tvDescription)

        //recupera dal db gli attributi dell'evento selezionato
        val event = db.selectEvent(id)

        //assegna i valori dell'evento alle TextView corrispondenti
        findViewById<TextView>(R.id.tvName).text = event.name
        findViewById<TextView>(R.id.tvStartDate).text = event.startDate + " " + event.startTime
        findViewById<TextView>(R.id.tvEndDate).text = event.endDate + " " + event.endTime
        tvDescription.text = event.description

        if (event.description.isNullOrEmpty()){
            //se la descrizione dell'evento non è stata inserita nasconde il campo corrispondente
            tvDescription.isVisible = false
        }

        findViewById<FloatingActionButton>(R.id.btnBack).setOnClickListener {
            //torna alla main activity
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
        }

        findViewById<FloatingActionButton>(R.id.btnDelete).setOnClickListener {
            //elimina l'evento selezionato
            val builder = AlertDialog.Builder(this) //dialog di conferma
            with(builder){
                setMessage(resources.getText(R.string.dialog_delete))
                setPositiveButton(resources.getText(R.string.dialog_button_ok)) { dialog, thisId ->
                    db.remove(id)
                    val builder = AlertDialog.Builder(this@DisplayEventActivity) //dialog per l'invio di email di notifica
                    with(builder){
                        setMessage(resources.getText(R.string.dialog_deleted_ok))
                        setPositiveButton(resources.getText(R.string.dialog_button_ok)){dialog, thisId ->
                            //avvia l'activity per l'invio di email di notifica
                            val intent = Intent(applicationContext, SendEmailActivity::class.java)
                            intent.putExtra("SUBJECT", defaultSubject.toString() + findViewById<TextView>(R.id.tvName).text)
                            startActivity(intent)
                        }
                        setNegativeButton(resources.getText(R.string.dialog_button_cancel)){ dialog, thisId ->
                            //torna alla main activity
                            startActivity(Intent(applicationContext, MainActivity::class.java))
                        }
                        show()
                    }
                }
                setNegativeButton(resources.getText(R.string.dialog_button_cancel), null)
                show()
            }
        }

        findViewById<FloatingActionButton>(R.id.btnModify).setOnClickListener {
            //avvia l'activity di creazione/modifica eventi specificando che si tratta di un'operazione di modifica di un evento esistente
            val intent = Intent(applicationContext, RegisterEventActivity::class.java)
            intent.putExtra("FUNCTION", "Update")
            intent.putExtra("ID", id)
            startActivity(intent)
        }
    }
}