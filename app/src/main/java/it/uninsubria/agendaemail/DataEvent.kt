package it.uninsubria.agendaemail

data class DataEvent(
    var id: String,
    var name: String,
    var startDate: String,
    var endDate: String,
    var startTime: String,
    var endTime: String,
    var description: String?,
){
    constructor() : this ("", "", "", "", "", "", "")

    override fun toString(): String {
        return name
    }

    fun set(event: DataEvent?){
        id = event?.id.toString()
        name = event?.name.toString()
        startDate = event?.startDate.toString()
        endDate = event?.endDate.toString()
        startTime = event?.startTime.toString()
        endTime = event?.endTime.toString()
        description = event?.description
    }
}